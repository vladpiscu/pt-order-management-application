package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public JTable createTable(List<Object> objects)
	{
		JTable table = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		Field[] fields = type.getDeclaredFields();
		Object[] columnsName = new Object[fields.length];
		for(int i = 0; i < fields.length; i++)
			columnsName[i] = (Object) fields[i].getName();
		model.setColumnIdentifiers(columnsName);
		Object[] rowData = new Object[fields.length];
		Method[] m = type.getDeclaredMethods();
		Method[] getters = new Method[fields.length];
		for(int i = 0; i < fields.length; i++)
		{
			for(int j = 0; j < m.length; j++)
			{
				if(m[j].getName().equalsIgnoreCase("get" + fields[i].getName()))
				{
					getters[i] = m[j];
				}
			}
		}
		
		for(int i = 0; i < objects.size(); i++)
		{
			for(int j = 0; j < fields.length; j++)
			{
				try {
					rowData[j] = (Object) getters[j].invoke(objects.get(i));
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
			model.addRow(rowData);
		}
		table.setModel(model);
		return table;
	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ecommerce.");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		
		ResultSet resultSet = null;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement("SELECT * FROM ecommerce." + type.getSimpleName());
			System.out.println(statement.toString());
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			List<T> l = createObjects(resultSet);
			if(l.isEmpty())
				return null;
			return l.get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + " DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public T insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		
		Field f;
		try {
			f = t.getClass().getDeclaredField("id");
			f.setAccessible(true);
			
			//Casuta eroare de pus

			
		} catch (NoSuchFieldException e) {
			//e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ecommerce.");
		sb.append(t.getClass().getSimpleName());
		sb.append(" VALUES ( ");
		for (Field field : t.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(t);
				if(field.getName().equalsIgnoreCase("id") == false)
				{
					if(field.getAnnotatedType().getType().getTypeName().contains("String"))
						sb.append("\"" + value + "\", ");
					else
						sb.append(value + ", ");
				}
				else
				{
					sb.append("NULL, ");
				}

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		sb.delete(sb.length() - 2, sb.length() - 1);
		sb.append(");");
		System.out.println(sb.toString());
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Input is incorect!");
		}
		
		return t;
	}

	public T update(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		int id = 0;
		try {
			Field f = t.getClass().getDeclaredField("id");
			f.setAccessible(true);
			id = f.getInt(t);
			
			//casuta eroare de pus
			if(findById(f.getInt(t)) == null)
			{
				return null;
			}
			
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ecommerce.");
		sb.append(t.getClass().getSimpleName());
		sb.append(" SET ");
		
		for (Field field : t.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			Object value;
			try {
				value = field.get(t);
				if(field.getAnnotatedType().getType().getTypeName().contains("String") )
					sb.append(field.getName() + " = \"" + value + "\", ");
				else
					sb.append(field.getName() + " = " + value + ", ");

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		sb.delete(sb.length() - 2, sb.length() - 1);
		sb.append(" WHERE id =");
		sb.append(" " + id);
		
		
		sb.append(";");
		System.out.println(sb.toString());
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return t;
	}
	
	public void delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;

		//casuta eroare de pus
		if(findById(id) == null)
		{
			return;
		}
			
		
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE from ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE id = " + id);
		sb.append(";");
		
		System.out.println(sb.toString());
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(sb.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}