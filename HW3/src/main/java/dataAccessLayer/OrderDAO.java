package dataAccessLayer;
import java.io.IOException;
import java.io.PrintWriter;

import model.*;

public class OrderDAO extends AbstractDAO<Order>{
	
	public OrderDAO()
	{
		super();
	}
	
	public void createBill(Customer cust, Product prod)
	{
		try{
			String fileName = new String("Bill" + this.findAll().size() + ".txt");
		    PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		    writer.println("Name: " + cust.getName() + "\n");
		    writer.println("Email: " + cust.getEmail() + "\n");
		    writer.println("Product: " + prod.getName() + "\n");
		    writer.println("Price: " + prod.getPrice() + "\n");
		    writer.println("Quantity: " + prod.getQuantity() + "\n");
		    writer.println("Total Price: " + prod.getPrice() * prod.getQuantity() + "\n");
		    writer.close();
		} catch (IOException e) {
		   
		}
	}
	
	
	
}
