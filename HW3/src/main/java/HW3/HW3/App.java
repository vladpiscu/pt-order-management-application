package HW3.HW3;

import java.util.ArrayList;
import java.util.List;

import businessLayer.OrderBLL;
import gui.*;

import dataAccessLayer.*;
import model.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ProductDAO p = new ProductDAO();
    	GUI g = new GUI();
    	List<Object> objects = new ArrayList<Object>(p.findAll());
    	g.setTableProducts(p.createTable(objects));
    	g.setVisible(true);
    	Controller c = new Controller(g);
    	
    	//g.setMenu(true);
    	//g.setVisible(true);
    	
    	//CustomerDAO c = new CustomerDAO();
    	//OrderDAO o = new OrderDAO();
    	//Customer cust = new Customer(2, "Piscu Andra", "Ion Buteanu", "0744312212", 1, "andrapiscu@gmail.com");
    	
    	//c.delete(1);
    	//c.update(cust);
    	//c.insert(cust);
    	//c.findAll();
    	//c.findById(1);
    	//OrderBLL o = new OrderBLL();
    	//Order ord = new Order(1, 1, 2, 3);
    	//o.insertOrder(ord);
    	//System.out.println(c.findById(1).getName());
    }
}
