package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import businessLayer.*;
import dataAccessLayer.CustomerDAO;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Customer;
import model.Order;
import model.Product;

public class Controller {
	private GUI g;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addOrder(new Ord());
		this.g.addCustomers(new Cust());
		this.g.addProducts(new Prod());
		this.g.addMenu(new Menu());
		this.g.addMenu1(new Menu());
		this.g.addMenu2(new Menu());
		this.g.addInsertCustomer(new InsertCust());
		this.g.addDeleteCustomer(new DeleteCust());
		this.g.addUpdateCustomer(new UpdateCust());
		this.g.addFindCustomer(new SearchCust());
		this.g.addInsertProduct(new InsertProd());
		this.g.addDeleteProduct(new DeleteProd());
		this.g.addUpdateProduct(new UpdateProd());
		this.g.addFindProduct(new SearchProd());
		this.g.addAddOrder(new MakeOrder());
		this.g.addShowCustomers(new ShowCustomer());
		this.g.addShowProducts(new ShowProduct());
	}
	
	public class ShowCustomer implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			CustomerDAO p = new CustomerDAO();
			CustomerBLL c = new CustomerBLL();
			c.findAllCustomer();
			List<Object> objects = new ArrayList<Object>();
			objects.addAll(c.findAllCustomer());
	    	g.setTableProducts(p.createTable(objects));
		}
	}
	
	public class ShowProduct implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			ProductDAO p = new ProductDAO();
			ProductBLL c = new ProductBLL();
			c.findAllProduct();
			List<Object> objects = new ArrayList<Object>();
			objects.addAll(c.findAllProduct());
	    	g.setTableProducts(p.createTable(objects));
		}
	}
	
	public class MakeOrder implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			ProductBLL p = new ProductBLL();
			CustomerBLL c = new CustomerBLL();
			Product prod = p.findProductById(g.getProductId());
			Customer cust = c.findCustomerById(g.getCustomerId());
			OrderBLL o = new OrderBLL();
			OrderDAO oDAO = new OrderDAO();
			Order ord = new Order(1, cust.getId(), prod.getId(), Integer.parseInt(g.getOrderQuantity()));
			o.insertOrder(ord);
		}
	}
	
	public class SearchProd implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			ProductDAO c = new ProductDAO();
			ProductBLL p = new ProductBLL();
			List<Object> objects = new ArrayList<Object>();
			objects.add((Object) p.findProductById(Integer.parseInt(g.getProdId())));
	    	g.setTableProducts(c.createTable(objects));
		}
	}
	
	public class UpdateProd implements ActionListener
	{
		public Product getProduct()
		{
			try{
				Product c = new Product(Integer.parseInt(g.getProdId()), g.getProdName(), Integer.parseInt(g.getProdPrice()), g.getBrandId(), Integer.parseInt(g.getProdQuantity()));
				return c;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			ProductDAO c = new ProductDAO();
			ProductBLL p = new ProductBLL();
			p.updateProduct(this.getProduct());
			List<Object> objects = new ArrayList<Object>(p.findAllProduct());
	    	g.setTableProducts(c.createTable(objects));
		}
	}
	
	public class DeleteProd implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			ProductDAO c = new ProductDAO();
			ProductBLL p = new ProductBLL();
			c.delete(Integer.parseInt(g.getProdId()));
			List<Object> objects = new ArrayList<Object>(p.findAllProduct());
	    	g.setTableProducts(c.createTable(objects));
		}
	}
	
	public class InsertProd implements ActionListener
	{
		public Product getProduct()
		{
			try{
				Product c = new Product(Integer.parseInt(g.getProdId()), g.getProdName(), Integer.parseInt(g.getProdPrice()), g.getBrandId(), Integer.parseInt(g.getProdQuantity()));
				return c;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			ProductBLL c = new ProductBLL();
			ProductDAO p = new ProductDAO();
			c.insertProduct(this.getProduct());
			List<Object> objects = new ArrayList<Object>(c.findAllProduct());
	    	g.setTableProducts(p.createTable(objects));
		}
	}
	
	public class SearchCust implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			CustomerDAO c = new CustomerDAO();
			CustomerBLL b = new CustomerBLL();
			List<Object> objects = new ArrayList<Object>();
			objects.add((Object) b.findCustomerById(Integer.parseInt(g.getCustId())));
	    	g.setTableCustomers(c.createTable(objects));
		}
	}
	
	public class UpdateCust implements ActionListener
	{
		public Customer getCustomer()
		{
			try{
				Customer c = new Customer(Integer.parseInt(g.getCustId()), g.getCustName(), g.getCustStreet(), g.getPhone(), g.getCityId(), g.getEmail());
				return c;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			CustomerDAO c = new CustomerDAO();
			CustomerBLL b = new CustomerBLL();
			b.updateCustomer(this.getCustomer());
			List<Object> objects = new ArrayList<Object>(b.findAllCustomer());
	    	g.setTableCustomers(c.createTable(objects));
		}
	}
	
	public class DeleteCust implements ActionListener
	{
		
		public void actionPerformed(ActionEvent arg0) {
			CustomerDAO c = new CustomerDAO();
			c.delete(Integer.parseInt(g.getCustId()));
			List<Object> objects = new ArrayList<Object>(c.findAll());
	    	g.setTableCustomers(c.createTable(objects));
		}
	}
	
	public class InsertCust implements ActionListener
	{
		public Customer getCustomer()
		{
			try{
				Customer c = new Customer(Integer.parseInt(g.getCustId()), g.getCustName(), g.getCustStreet(), g.getPhone(), g.getCityId(), g.getEmail());
				return c;
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "Input is incorect!");
			}
			return null;
		}
		
		public void actionPerformed(ActionEvent arg0) {
			CustomerDAO c = new CustomerDAO();
			CustomerBLL b = new CustomerBLL();
			b.insertCustomer(this.getCustomer());
			List<Object> objects = new ArrayList<Object>(b.findAllCustomer());
	    	g.setTableCustomers(c.createTable(objects));
		}
	}
	
	public class Menu implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(false);
			g.setProdPanel(false);
			g.setMenu(true);
		}
	}
	
	public class Ord implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(true);
			g.setCustPanel(false);
			g.setProdPanel(false);
			g.setMenu(false);
		}
	}
	
	public class Cust implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(true);
			g.setProdPanel(false);
			g.setMenu(false);
			CustomerDAO c = new CustomerDAO();
			List<Object> objects = new ArrayList<Object>(c.findAll());
	    	g.setTableCustomers(c.createTable(objects));
		}
	}
	
	public class Prod implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) {
			g.setOrdPanel(false);
			g.setCustPanel(false);
			g.setProdPanel(true);
			g.setMenu(false);
			ProductDAO p = new ProductDAO();
			List<Object> objects = new ArrayList<Object>(p.findAll());
	    	g.setTableProducts(p.createTable(objects));
		}
	}
}
