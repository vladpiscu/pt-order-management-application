package gui;

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import dataAccessLayer.*;
import model.Brand;
import model.City;
import model.Customer;
import model.Product;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JPanel prodPanel;
	private JPanel custPanel;
	private JPanel ordPanel;
	private JPanel menu;
	private JButton btnProducts;
	private JButton btnCustomers;
	private JButton btnOrder;
	private JButton btnMenu;
	private JButton btnInsertCustomer;
	private JButton btnUpdateCustomer;
	private JTextField textId;
	private JTextField textName;
	private JTextField textStreet;
	private JTextField textPhone;
	private JTextField textEmail;
	private JComboBox<City> comboBox_1;
	private JButton btnDeleteCustomer;
	private JButton btnSearchCustomer;
	private JLabel label;
	private JTextField textProdId;
	private JLabel label_1;
	private JTextField textProdName;
	private JLabel lblPrice;
	private JTextField textPrice;
	private JLabel lblBrand;
	private JComboBox<Brand> comboBox;
	private JLabel lblQuantity;
	private JTextField textQuantity;
	private JButton btnInsertProduct;
	private JButton btnDeleteProduct;
	private JButton btnUpdateProduct;
	private JButton btnSearchProduct;
	private JButton buttonMenu1;
	private JComboBox comboBox_2;
	private JLabel lblNewLabel;
	private JComboBox comboBox_3;
	private JLabel lblSelectAProduct;
	private JTextField textField;
	private JLabel lblNewLabel_1;
	private JButton btnAddOrder;
	private JButton buttonMenu2;
	private JButton btnShowCustomers;
	private JButton btnShowProducts;
	private JLabel lblToMakeAn;
	private JLabel lblAndIntroduceA;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 997, 1071);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		menu = new JPanel();
		contentPane.add(menu, "name_180132812520410");
		menu.setLayout(null);
		setTitle("Warehouse");
		
		btnProducts = new JButton("Products");
		btnProducts.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnProducts.setBounds(568, 256, 255, 61);
		menu.add(btnProducts);
		
		btnCustomers = new JButton("Customers");
		btnCustomers.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnCustomers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnCustomers.setBounds(128, 256, 260, 61);
		menu.add(btnCustomers);
		
		btnOrder = new JButton("Make order");
		btnOrder.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnOrder.setBounds(333, 381, 293, 77);
		menu.add(btnOrder);
		
		custPanel = new JPanel();
		contentPane.add(custPanel, "name_180132824672168");
		custPanel.setLayout(null);
		
		btnMenu = new JButton("Menu");
		btnMenu.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnMenu.setBounds(115, 895, 235, 62);
		custPanel.add(btnMenu);
		
		btnInsertCustomer = new JButton("Insert Customer");
		btnInsertCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnInsertCustomer.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnInsertCustomer.setBounds(75, 706, 322, 62);
		custPanel.add(btnInsertCustomer);
		
		btnUpdateCustomer = new JButton("Update Customer");
		btnUpdateCustomer.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnUpdateCustomer.setBounds(563, 706, 322, 62);
		custPanel.add(btnUpdateCustomer);
		
		textId = new JTextField();
		textId.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textId.setBounds(114, 546, 236, 39);
		custPanel.add(textId);
		textId.setColumns(10);
		
		JLabel lblId = new JLabel("Id:");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblId.setBounds(203, 508, 115, 33);
		custPanel.add(lblId);
		
		textName = new JTextField();
		textName.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textName.setColumns(10);
		textName.setBounds(367, 546, 236, 39);
		custPanel.add(textName);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblName.setBounds(456, 508, 115, 33);
		custPanel.add(lblName);
		
		textStreet = new JTextField();
		textStreet.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textStreet.setColumns(10);
		textStreet.setBounds(616, 545, 236, 39);
		custPanel.add(textStreet);
		
		JLabel lblStreet = new JLabel("Street:");
		lblStreet.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblStreet.setBounds(686, 508, 115, 33);
		custPanel.add(lblStreet);
		
		textPhone = new JTextField();
		textPhone.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textPhone.setColumns(10);
		textPhone.setBounds(114, 651, 236, 39);
		custPanel.add(textPhone);
		
		JLabel lblPhone = new JLabel("Phone:");
		lblPhone.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblPhone.setBounds(203, 613, 115, 33);
		custPanel.add(lblPhone);
		
		textEmail = new JTextField();
		textEmail.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textEmail.setColumns(10);
		textEmail.setBounds(367, 649, 236, 39);
		custPanel.add(textEmail);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblEmail.setBounds(437, 613, 115, 33);
		custPanel.add(lblEmail);
		
		CityDAO cities = new CityDAO();
		List<City> objects = new ArrayList<City>(cities.findAll());
		
		comboBox_1 = new JComboBox(objects.toArray());
		comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 30));
		comboBox_1.setBounds(616, 649, 236, 39);
		//for(int i = 0; i < objects.size(); i++)
			//comboBox_1.addItem(objects.get(i));
		custPanel.add(comboBox_1);
		
		JLabel lblCity = new JLabel("City:");
		lblCity.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblCity.setBounds(701, 612, 115, 33);
		custPanel.add(lblCity);
		
		btnDeleteCustomer = new JButton("Delete Customer");
		btnDeleteCustomer.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnDeleteCustomer.setBounds(75, 804, 322, 62);
		custPanel.add(btnDeleteCustomer);
		
		btnSearchCustomer = new JButton("Search Customer");
		btnSearchCustomer.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnSearchCustomer.setBounds(563, 804, 322, 62);
		custPanel.add(btnSearchCustomer);
		
		btnShowCustomers = new JButton("Show customers");
		btnShowCustomers.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnShowCustomers.setBounds(563, 894, 322, 62);
		custPanel.add(btnShowCustomers);
		
		prodPanel = new JPanel();
		contentPane.add(prodPanel, "name_180132836834188");
		prodPanel.setLayout(null);
		
		label = new JLabel("Id:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 34));
		label.setBounds(209, 510, 115, 33);
		prodPanel.add(label);
		
		textProdId = new JTextField();
		textProdId.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textProdId.setColumns(10);
		textProdId.setBounds(120, 548, 236, 39);
		prodPanel.add(textProdId);
		
		label_1 = new JLabel("Name:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		label_1.setBounds(462, 510, 115, 33);
		prodPanel.add(label_1);
		
		textProdName = new JTextField();
		textProdName.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textProdName.setColumns(10);
		textProdName.setBounds(373, 548, 236, 39);
		prodPanel.add(textProdName);
		
		lblPrice = new JLabel("Price:");
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblPrice.setBounds(692, 510, 115, 33);
		prodPanel.add(lblPrice);
		
		textPrice = new JTextField();
		textPrice.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textPrice.setColumns(10);
		textPrice.setBounds(622, 547, 236, 39);
		prodPanel.add(textPrice);
		
		lblBrand = new JLabel("Brand:");
		lblBrand.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblBrand.setBounds(607, 615, 115, 33);
		prodPanel.add(lblBrand);
		
		BrandDAO brands = new BrandDAO();
		List<Brand> brandObjects = new ArrayList<Brand>(brands.findAll());
		
		comboBox = new JComboBox(brandObjects.toArray());
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 30));
		comboBox.setBounds(540, 652, 236, 39);
		prodPanel.add(comboBox);
		
		lblQuantity = new JLabel("Quantity:");
		lblQuantity.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblQuantity.setBounds(256, 615, 157, 33);
		prodPanel.add(lblQuantity);
		
		textQuantity = new JTextField();
		textQuantity.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textQuantity.setColumns(10);
		textQuantity.setBounds(209, 654, 236, 39);
		prodPanel.add(textQuantity);
		
		btnInsertProduct = new JButton("Insert Product");
		btnInsertProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnInsertProduct.setBounds(81, 708, 322, 62);
		prodPanel.add(btnInsertProduct);
		
		btnDeleteProduct = new JButton("Delete Product");
		btnDeleteProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnDeleteProduct.setBounds(81, 806, 322, 62);
		prodPanel.add(btnDeleteProduct);
		
		btnUpdateProduct = new JButton("Update Product");
		btnUpdateProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnUpdateProduct.setBounds(569, 708, 322, 62);
		prodPanel.add(btnUpdateProduct);
		
		btnSearchProduct = new JButton("Search Product");
		btnSearchProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnSearchProduct.setBounds(569, 806, 322, 62);
		prodPanel.add(btnSearchProduct);
		
		buttonMenu1 = new JButton("Menu");
		buttonMenu1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		buttonMenu1.setBounds(121, 896, 235, 62);
		prodPanel.add(buttonMenu1);
		
		btnShowProducts = new JButton("Show Products");
		btnShowProducts.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnShowProducts.setBounds(569, 896, 322, 62);
		prodPanel.add(btnShowProducts);
		
		ordPanel = new JPanel();
		contentPane.add(ordPanel, "name_180132848306675");
		ordPanel.setLayout(null);
		
		CustomerDAO cust = new CustomerDAO();
		List<Customer> custObjects = new ArrayList<Customer>(cust.findAll());
		
		comboBox_2 = new JComboBox(custObjects.toArray());
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 34));
		comboBox_2.setBounds(84, 379, 351, 67);
		ordPanel.add(comboBox_2);
		
		lblNewLabel = new JLabel("Select a customer:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblNewLabel.setBounds(115, 315, 322, 52);
		ordPanel.add(lblNewLabel);
		
		ProductDAO prod = new ProductDAO();
		List<Product> prodObjects = new ArrayList<Product>(prod.findAll());
		
		comboBox_3 = new JComboBox(prodObjects.toArray());
		comboBox_3.setFont(new Font("Tahoma", Font.PLAIN, 34));
		comboBox_3.setBounds(541, 379, 371, 67);
		ordPanel.add(comboBox_3);
		
		lblSelectAProduct = new JLabel("Select a product:");
		lblSelectAProduct.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblSelectAProduct.setBounds(590, 315, 322, 52);
		ordPanel.add(lblSelectAProduct);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 34));
		textField.setBounds(370, 543, 236, 67);
		ordPanel.add(textField);
		textField.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Quantity:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblNewLabel_1.setBounds(413, 480, 193, 52);
		ordPanel.add(lblNewLabel_1);
		
		btnAddOrder = new JButton("Add Order");
		btnAddOrder.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnAddOrder.setBounds(630, 743, 236, 95);
		ordPanel.add(btnAddOrder);
		
		buttonMenu2 = new JButton("Menu");
		buttonMenu2.setFont(new Font("Tahoma", Font.PLAIN, 34));
		buttonMenu2.setBounds(113, 743, 235, 95);
		ordPanel.add(buttonMenu2);
		
		lblToMakeAn = new JLabel("To make an order, select a customer, a product");
		lblToMakeAn.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblToMakeAn.setBounds(113, 70, 754, 52);
		ordPanel.add(lblToMakeAn);
		
		lblAndIntroduceA = new JLabel("and introduce a quantity:");
		lblAndIntroduceA.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblAndIntroduceA.setBounds(311, 134, 754, 52);
		ordPanel.add(lblAndIntroduceA);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 16, 903, 451);
		scrollPane.createVerticalScrollBar();
		
		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(26, 16, 903, 451);
		scrollPane1.createVerticalScrollBar();
		
		
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 34));
		
	}
	
	public void setTableProducts(JTable t)
	{
		table = t;
		table.setFont(new Font("Tahoma", Font.PLAIN, 34));
		table.setRowHeight(100);
		table.doLayout();
		scrollPane.setViewportView(table);
		prodPanel.add(scrollPane);
	}
	
	public void setTableCustomers(JTable t)
	{
		table = t;
		table.setFont(new Font("Tahoma", Font.PLAIN, 34));
		table.setRowHeight(100);
		table.doLayout();
		scrollPane1.setViewportView(table);
		custPanel.add(scrollPane1);
		
	}
	
	public void addShowCustomers(ActionListener a)
	{
		btnShowCustomers.addActionListener(a);
	}
	
	public void addShowProducts(ActionListener a)
	{
		btnShowProducts.addActionListener(a);
	}
	
	public void addOrder(ActionListener a)
	{
		btnOrder.addActionListener(a);
	}
	
	public void addCustomers(ActionListener a)
	{
		btnCustomers.addActionListener(a);
	}
	
	public void addProducts(ActionListener a)
	{
		btnProducts.addActionListener(a);
	}
	
	public void addMenu(ActionListener a)
	{
		btnMenu.addActionListener(a);
	}
	
	public void addMenu1(ActionListener a)
	{
		buttonMenu1.addActionListener(a);
	}
	
	public void addMenu2(ActionListener a)
	{
		buttonMenu2.addActionListener(a);
	}
	
	public void addAddOrder(ActionListener a)
	{
		btnAddOrder.addActionListener(a);
	}
	
	public void addInsertCustomer(ActionListener a)
	{
		btnInsertCustomer.addActionListener(a);
	}
	
	public void addDeleteCustomer(ActionListener a)
	{
		btnDeleteCustomer.addActionListener(a);
	}
	
	public void addUpdateCustomer(ActionListener a)
	{
		btnUpdateCustomer.addActionListener(a);
	}
	
	public void addFindCustomer(ActionListener a)
	{
		btnSearchCustomer.addActionListener(a);
	}
	
	public void addInsertProduct(ActionListener a)
	{
		btnInsertProduct.addActionListener(a);
	}
	
	public void addDeleteProduct(ActionListener a)
	{
		btnDeleteProduct.addActionListener(a);
	}
	
	public void addUpdateProduct(ActionListener a)
	{
		btnUpdateProduct.addActionListener(a);
	}
	
	public void addFindProduct(ActionListener a)
	{
		btnSearchProduct.addActionListener(a);
	}
	
	public void setMenu(boolean b)
	{
		menu.setVisible(b);
	}
	
	public void setOrdPanel(boolean b)
	{
		ordPanel.setVisible(b);
	}
	
	public void setProdPanel(boolean b)
	{
		prodPanel.setVisible(b);
	}
	
	public void setCustPanel(boolean b)
	{
		custPanel.setVisible(b);
	}
	
	public String getCustId()
	{
		return textId.getText();
	}
	
	public String getCustName()
	{
		return textName.getText();
	}
	
	public String getCustStreet()
	{
		return textStreet.getText();
	}
	
	public String getPhone()
	{
		return textPhone.getText();
	}
	
	public String getEmail()
	{
		return textEmail.getText();
	}
	
	public int getCityId()
	{
		City city = (City) comboBox_1.getSelectedItem();
		return city.getId();
	}
	
	public int getBrandId()
	{
		Brand brand = (Brand) comboBox.getSelectedItem();
		return brand.getId();
	}
	
	public int getCustomerId()
	{
		Customer cust = (Customer) comboBox_2.getSelectedItem();
		return cust.getId();
	}
	
	public int getProductId()
	{
		Product prod = (Product) comboBox_3.getSelectedItem();
		return prod.getId();
	}
	
	public String getProdId()
	{
		return textProdId.getText();
	}
	
	public String getProdName()
	{
		return textProdName.getText();
	}
	
	public String getProdPrice()
	{
		return textPrice.getText();
	}
	
	public String getProdQuantity()
	{
		return textQuantity.getText();
	}
	
	public String getOrderQuantity()
	{
		return textField.getText();
	}
}
