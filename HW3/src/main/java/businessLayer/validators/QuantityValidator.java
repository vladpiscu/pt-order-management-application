package businessLayer.validators;

import javax.swing.JOptionPane;

import model.Product;

public class QuantityValidator implements Validator<Product>{

	public void validate(Product t) {
		if(t.getQuantity() < 0)
		{
			JOptionPane.showMessageDialog(null, "Under stock!");
			throw new IllegalArgumentException("It isn't a valid quantity.");
		}
	}

}
