package businessLayer.validators;

import model.Product;

public class PriceValidator implements Validator<Product>{

	public void validate(Product t) {
		if(t.getPrice() <= 0)
			throw new IllegalArgumentException("It isn't a valid price.");
		
	}

}
