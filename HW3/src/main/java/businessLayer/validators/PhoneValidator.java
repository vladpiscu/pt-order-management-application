package businessLayer.validators;

import model.Customer;

public class PhoneValidator implements Validator<Customer>{

	public void validate(Customer t) {
		if(t.getPhoneNo().length() != 10)
			throw new IllegalArgumentException("It isn't a valid phone number.");
		else
		{
			try{
				Integer.parseInt(t.getPhoneNo());
			}
			catch (NumberFormatException e)
			{
				throw new IllegalArgumentException("It isn't a valid phone number.");
			}
		}
			
		
	}
		
}
