package businessLayer;

import businessLayer.validators.*;
import java.util.*;
import model.*;
import dataAccessLayer.*;

public class OrderBLL {
	private List<Validator<Product>> validators;
	private ProductDAO p;
	private CustomerDAO c;
	private OrderDAO o;
	
	public OrderBLL()
	{
		validators = new ArrayList<Validator<Product>>();
		validators.add(new QuantityValidator());
		c = new CustomerDAO();
		p =new ProductDAO();
		o = new OrderDAO();
	}
	
	
	public Order insertOrder(Order ord)
	{
		Customer cust = c.findById(ord.getCustId());
		Product prod = p.findById(ord.getProdId());
		prod.setQuantity(prod.getQuantity() - ord.getQuantity());
		for(Validator<Product> v : validators)
			v.validate(prod);
		p.update(prod);
		o.createBill(cust, prod);
		return o.insert(ord);
	}
	
}
