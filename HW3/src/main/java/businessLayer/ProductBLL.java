package businessLayer;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import businessLayer.validators.PriceValidator;
import businessLayer.validators.QuantityValidator;
import businessLayer.validators.Validator;
import dataAccessLayer.ProductDAO;
import model.Product;

public class ProductBLL {
	private List<Validator<Product>> validators;
	private ProductDAO c;
	
	public ProductBLL()
	{
		validators = new ArrayList<Validator<Product>>();
		validators.add(new PriceValidator());
		validators.add(new QuantityValidator());
		c = new ProductDAO();
	}
	
	public Product findProductById(int id)
	{
		Product prod = c.findById(id);
		if(prod == null)
		{
			JOptionPane.showMessageDialog(null, "Id doesn't exist!");
			throw new IllegalArgumentException("The product was not found");
		}
		return prod;
	}
	
	public List<Product> findAllProduct()
	{
		return c.findAll();
	}
	
	public Product insertProduct(Product prod)
	{
		for(Validator<Product> v : validators)
			v.validate(prod);
		return c.insert(prod);
	}
	
	public Product updateProduct(Product prod)
	{
		for(Validator<Product> v : validators)
			v.validate(prod);
		return c.update(prod);
	}

}
