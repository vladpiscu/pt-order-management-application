package businessLayer;

import businessLayer.validators.*;
import java.util.*;

import javax.swing.JOptionPane;

import model.Customer;
import dataAccessLayer.CustomerDAO;

public class CustomerBLL {
	private List<Validator<Customer>> validators;
	private CustomerDAO c;
	
	public CustomerBLL()
	{
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new PhoneValidator());
		c = new CustomerDAO();
	}
	
	public Customer findCustomerById(int id)
	{
		Customer cust = c.findById(id);
		if(cust == null)
		{
			JOptionPane.showMessageDialog(null, "Id doesn't exist!");
			throw new IllegalArgumentException("The student was not found");
		}
		return cust;
	}
	
	public List<Customer> findAllCustomer()
	{
		return c.findAll();
	}
	
	public Customer insertCustomer(Customer cust)
	{
		for(Validator<Customer> v : validators)
			v.validate(cust);
		return c.insert(cust);
	}
	
	public Customer updateCustomer(Customer cust)
	{
		for(Validator<Customer> v : validators)
			v.validate(cust);
		return c.update(cust);
	}
}
