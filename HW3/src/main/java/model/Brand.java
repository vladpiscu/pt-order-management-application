package model;

public class Brand{
	private int id;
	private String name;
	
	public Brand()
	{
		id = 0;
		name = "";
	}
	
	public Brand(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return this.name;
	}
}
