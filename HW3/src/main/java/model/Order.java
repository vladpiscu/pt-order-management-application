package model;

public class Order {
	private int id;
	private int custId;
	private int prodId;
	private int quantity;
	
	public Order()
	{
		id = 0;
		custId = 0;
		prodId = 0;
		quantity = 0;
	}
	
	public Order(int ordId, int custId, int prId, int q)
	{
		this.id = ordId;
		this.custId = custId;
		prodId = prId;
		quantity = q;
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getCustId()
	{
		return custId;
	}
	
	public int getProdId()
	{
		return prodId;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public void setProdId(int id)
	{
		prodId = id;
	}
	
	public void setQuantity(int q)
	{
		quantity = q;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setCustId(int id)
	{
		custId = id;
	}
	

}
