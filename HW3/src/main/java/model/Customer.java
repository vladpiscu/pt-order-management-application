package model;

public class Customer {
	private int id;
	private String name;
	private String street;
	private String phoneNo;
	private int cityId;
	private String email;
	
	public Customer()
	{
		id = 0;
		name = "";
		street = "";
		phoneNo = "";
		cityId = 0;
		email = "";
	}
	
	public Customer(int id, String name, String street, String phoneNo, int cityId, String email)
	{
		this.id = id;
		this.name = name;
		this.street = street;
		this.phoneNo = phoneNo;
		this.cityId = cityId;
		this.email = email;
	}
	
	public String getStreet()
	{
		return street;
	}
	
	public String getPhoneNo()
	{
		return phoneNo;
	}
	
	public int getCityId()
	{
		return cityId;
	}
	
	public void setStreet(String s)
	{
		street = s;
	}
	
	public void setPhoneNo(String s)
	{
		phoneNo = s;
	}
	
	public void setCityId(int id)
	{
		cityId = id;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setEmail(String e)
	{
		email = e;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public String toString()
	{
		return name;
	}
}
