package model;

public class Product{
	private int id;
	private String name;
	private int price;
	private int brandId;
	private int quantity;
	
	public Product()
	{
		id = 0;
		name = "";
		price = 0;
		brandId = 0;
		quantity = 0;
	}
	
	public Product(int id, String name, int p, int brand, int quantity)
	{
		this.id = id;
		this.name = name;
		price = p;
		brandId = brand;
		this.quantity = quantity;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public int getPrice()
	{
		return price;
	}
	
	public int getBrandId()
	{
		return brandId;
	}
	
	public void setPrice(int p)
	{
		price = p;
	}
	
	public void setBrandId(int id)
	{
		brandId = id;
	}
	
	public void setQuantity(int q)
	{
		quantity = q;
	}
	
	public int getQuantity()
	{
		return quantity;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String toString()
	{
		return name;
	}
	
}
