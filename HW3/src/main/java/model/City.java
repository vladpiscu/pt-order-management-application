package model;

public class City{
	private int id;
	private String name;
	private int countryId;
	
	public City()
	{
		id = 0;
		name = "";
		countryId = 0;
	}
	public City(int id, String name, int countryId)
	{
		this.id = id;
		this.name = name;
		this.countryId = countryId;
	}
	
	public int getCountryId()
	{
		return countryId;
	}
	
	public void setCountryId(int id)
	{
		countryId = id;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String toString()
	{
		return this.name;
	}
}
